create table if not exists user (
    id bigint not null,
    email varchar(255),
    fullname varchar(255),
    password varchar(255),
    username varchar(255),
    primary key (id)
);

create table if not exists goal (
    id bigint not null,
    active boolean not null,
    description varchar(255),
    user_id bigint,
    primary key (id),
    constraint FK_UserGoal foreign key (user_id) references user(id) on delete cascade
);


create table if not exists quest (
    id bigint not null,
    description varchar(255),
    content varchar(255),
    score int,
    active boolean not null,
    user_id bigint,
    goal_id bigint,
    primary key (id),
    constraint FK_UserQuest foreign key (user_id) references user(id) on delete cascade,
    constraint FK_GoalQuest foreign key (goal_id) references goal(id) on delete cascade
);
