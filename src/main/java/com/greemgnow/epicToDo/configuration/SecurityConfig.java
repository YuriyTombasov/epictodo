package com.greemgnow.epicToDo.configuration;

import com.greemgnow.epicToDo.repository.UserRepositoryUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    //@Autowired
    //private UserDetailsService userDetailsService;
    @Autowired
    private UserRepositoryUserDetailsService userDetailsService;

    @Bean
    public PasswordEncoder encoder() {
        return new StandardPasswordEncoder("53cr3t");
    }
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
            .userDetailsService(userDetailsService)
            .passwordEncoder(encoder());
    }
    
    
    @Override
    protected void configure(HttpSecurity http) throws Exception{
        /* default Spring Security configure(HttpSecurity http)
        protected void configure(HttpSecurity http) throws Exception {
            http
            .authorizeRequests()
            .anyRequest().authenticated()
            .and()
            .formLogin()
            .and()
            .httpBasic();
        }
        */
        
        http
            .authorizeRequests()
            .antMatchers("/user/**", "/quest/**").hasRole("USER") // may be authenticated?
            .antMatchers("/", "/login").permitAll()
            .and()
            .formLogin()
            .and()
            .logout().logoutSuccessUrl("/");

        
        // !!! this for dev stage !!!
        http.csrf().disable();
        http.headers().frameOptions().disable();

    }
    
    
    
}
