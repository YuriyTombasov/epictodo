package com.greemgnow.epicToDo.entity;

import com.greemgnow.epicToDo.dto.QuestDTO;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;


@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Quest implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    
    String description;
    
    String content;
    
    boolean active;
    
    int reward;
    
    @Temporal(TemporalType.TIMESTAMP)
    Date burnDate;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "goal_id")
    private Goal goal;

    public void fromDTO(QuestDTO questDTO){
        this.description = questDTO.getDescription();
        this.content = questDTO.getContent();
        this.active = questDTO.isActive();
        this.reward = questDTO.getReward();
        this.burnDate = questDTO.getBurnDate();
    }
    
    public QuestDTO toDTO(){
        QuestDTO dto = new QuestDTO();
        dto.setId(id);
        dto.setDescription(description);
        dto.setActive(active);
        dto.setReward(reward);
        dto.setBurnDate(burnDate);
        if(content != null){
            dto.setContent(content);
        }
        if(goal != null){
            dto.setGoal_id(goal.getId());
        }
        
        return dto;
    }

}
