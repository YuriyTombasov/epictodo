package com.greemgnow.epicToDo.entity;

import com.greemgnow.epicToDo.dto.GoalDTO;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;



@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Goal implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;
    
    String title;
    
    String description;
    
    boolean active;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;
    
    public void fromDTO(GoalDTO goalDTO){
        this.title = goalDTO.getTitle();
        this.description = goalDTO.getDescription();
        this.active = goalDTO.isActive();
    }
    
    public GoalDTO toDTO(){
        return new GoalDTO(id, title, description, active);
    }
    
}
