package com.greemgnow.epicToDo.repository;

import com.greemgnow.epicToDo.entity.Goal;
import com.greemgnow.epicToDo.entity.User;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;


public interface GoalRepository extends JpaRepository<Goal, Long>{
    
    List<Goal> findByUser(User user);
    
    Optional<Goal> findByIdAndUser(Long id, User user);
    
    
}
