package com.greemgnow.epicToDo.repository;

import com.greemgnow.epicToDo.entity.Quest;
import com.greemgnow.epicToDo.entity.User;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


public interface QuestRepository extends JpaRepository<Quest, Long>{
    
    List<Quest> findByUser(User user);
    
    Optional<Quest> findByIdAndUser(Long id, User user);
    
}
