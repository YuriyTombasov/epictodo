package com.greemgnow.epicToDo.dto;

import java.util.Date;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class QuestDTO {
    
    Long id;
    String description;
    String content;
    boolean active;
    int reward;
    Date burnDate;
    private Long goal_id;
    
}
