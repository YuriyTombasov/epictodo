package com.greemgnow.epicToDo.dto;

import lombok.Data;

@Data
public class EditPasswordDTO {
    
    String oldPwd;
    String newPwd;
    
    
}
