package com.greemgnow.epicToDo.controller;

import com.greemgnow.epicToDo.entity.User;
import com.greemgnow.epicToDo.repository.UserRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class MainController {

    @GetMapping()
    String greeting(){
        return "epicToDo main page";
    }
    
}
