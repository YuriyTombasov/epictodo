package com.greemgnow.epicToDo.controller;

import com.greemgnow.epicToDo.entity.User;
import com.greemgnow.epicToDo.repository.UserRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("registration")
public class RegistrationController {
    
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostMapping
    public ResponseEntity<User> createUser(@RequestBody User user){

        Optional<User> optionalUser = userRepository.findByUsername(user.getUsername()); //find user in DB
        
        if (optionalUser.isPresent()){
            return new ResponseEntity<>(user, HttpStatus.UNPROCESSABLE_ENTITY);//if user is already exists - do not save
        }
	
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setGold(0);
        return new ResponseEntity<>(userRepository.save(user), HttpStatus.CREATED);//else - save user. #DB will ignore PrimaryKey atribute in request body
    }
    

    
}
