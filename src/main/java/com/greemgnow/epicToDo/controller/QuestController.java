package com.greemgnow.epicToDo.controller;

import com.greemgnow.epicToDo.dto.QuestDTO;
import com.greemgnow.epicToDo.entity.Goal;
import com.greemgnow.epicToDo.entity.Quest;
import com.greemgnow.epicToDo.entity.User;
import com.greemgnow.epicToDo.repository.GoalRepository;
import com.greemgnow.epicToDo.repository.UserRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.Optional;
import org.springframework.web.bind.annotation.DeleteMapping;
import com.greemgnow.epicToDo.repository.QuestRepository;


@RestController
@RequestMapping("quest")
public class QuestController {
    
    @Autowired
    QuestRepository questRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    GoalRepository goalRepository;
    
    @GetMapping()
    public ResponseEntity<List<QuestDTO>> getQuestList(@AuthenticationPrincipal User currentUser){
        List<Quest> quests = questRepository.findByUser(currentUser);
        
        List<QuestDTO> questDTOs = new ArrayList<>();
        for(Quest quest : quests){
            questDTOs.add(quest.toDTO());
        }
        
        return new ResponseEntity<>(questDTOs, HttpStatus.OK);
    }
    
    @GetMapping("/{questId}")
    public ResponseEntity<QuestDTO> getQuest(@PathVariable Long questId, @AuthenticationPrincipal User currentUser){
        Optional<Quest> optQuest = questRepository.findByIdAndUser(questId, currentUser);
        if(!optQuest.isPresent())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            
        return new ResponseEntity<>(optQuest.get().toDTO(), HttpStatus.OK);
    }
    
    @PostMapping("/create")
    public ResponseEntity<QuestDTO> createQuest(@RequestBody QuestDTO questDTO, @AuthenticationPrincipal User currentUser){
        Quest quest = new Quest();
        quest.fromDTO(questDTO);
        quest.setUser(currentUser);
        
        //[T_I_goal.findByIdAndUser_1]
        Optional<Goal> optGoal = goalRepository.findByIdAndUser(questDTO.getGoal_id(), currentUser);
        quest.setGoal(optGoal.orElse(null));

        return new ResponseEntity<>(questRepository.save(quest).toDTO(), HttpStatus.CREATED);
    }
    
    @PutMapping("/edit")
    public ResponseEntity<QuestDTO> editQuest(@RequestBody QuestDTO questDTO, @AuthenticationPrincipal User currentUser){
        Optional<Quest> optQuest = questRepository.findByIdAndUser(questDTO.getId(), currentUser);
        if(!optQuest.isPresent())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        
        Quest quest = optQuest.get();
        boolean oldIsActive = quest.isActive(); //сохранил текущее значение active, чтобы узнать изменяется состояние (активен или выполнен) квеста или нет.
        int oldReward = quest.getReward(); //сохранил текущее reward
        quest.fromDTO(questDTO);
        
        //[T_M]
        //получаем вознаграждение за выполненный квест
        if(oldIsActive ^ questDTO.isActive()){
            int gold = 0;
            //Если квест выполняется и одновременно вознаграждение изменилось, то начисляем персонажу новое значение вознаграждения пришедшее из DTO (если не изменилось то старое).
            //Если квест был выполнен и возвращается в работу и одновременно поменяли вознаграждение - то отнимаем у персонажа ту сумму вознаграждения, которая была зачислена ранее при выполнении квеста.
            gold = (oldIsActive ? gold + quest.getReward() : gold - oldReward);
            
            User user = userRepository.findById(currentUser.getId()).get();
            user.takeReward(gold);
            userRepository.save(user);
        }
        //[1]Запрещено менять вознаграждение у выполненного квеста
        else if(!oldIsActive && (oldReward != questDTO.getReward())){
            quest.setReward(oldReward);
        }
        
        //[T_I_goal.findByIdAndUser_1]
        Optional<Goal> optGoal = goalRepository.findByIdAndUser(questDTO.getGoal_id(), currentUser);
        quest.setGoal(optGoal.orElse(null));
        
        return new ResponseEntity<>(questRepository.save(quest).toDTO(), HttpStatus.OK);
    }
    
    @DeleteMapping("/delete/{questId}")
    public ResponseEntity<Void> deleteQuest(@PathVariable Long questId, @AuthenticationPrincipal User currentUser){
        Optional<Quest> optQuest = questRepository.findByIdAndUser(questId, currentUser);
        if(!optQuest.isPresent())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            
        questRepository.deleteById(questId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    
}
