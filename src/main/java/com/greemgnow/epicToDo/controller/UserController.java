package com.greemgnow.epicToDo.controller;

import com.greemgnow.epicToDo.dto.EditPasswordDTO;
import com.greemgnow.epicToDo.entity.User;
import com.greemgnow.epicToDo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("user")
public class UserController {
    
    @Autowired
    UserRepository userRepository;
    
    @Autowired
    private PasswordEncoder passwordEncoder;
    
    //get current user
    @GetMapping
    public ResponseEntity<User> whatAUser(@AuthenticationPrincipal User currentUser){
        return new ResponseEntity<>(currentUser, HttpStatus.OK);
    }

    //may be edit this method for: userDTO or else same
    @PutMapping("/editProfile")
    public ResponseEntity<User> editUserProfile(@RequestBody User changesUser, @AuthenticationPrincipal User currentUser){
        User user = userRepository.findById(currentUser.getId()).get();
        user.setFullname(changesUser.getFullname());
        user.setEmail(changesUser.getEmail());
        return new ResponseEntity<>(userRepository.save(user), HttpStatus.OK);
    }
    
    @PutMapping("/changePassword")
    public ResponseEntity<Void> changePassword(@RequestBody EditPasswordDTO editPasswordDTO, @AuthenticationPrincipal User currentUser){
        if(!passwordEncoder.matches(editPasswordDTO.getOldPwd(), currentUser.getPassword()))
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
            
        User user = userRepository.findById(currentUser.getId()).get();
        user.setPassword(passwordEncoder.encode(editPasswordDTO.getNewPwd()));
        userRepository.save(user);

        SecurityContextHolder.getContext().getAuthentication().setAuthenticated(false);
        
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    //need normal buisness logic: genegate token on change pwd, send email, etc..
    @PutMapping("/forgotPassword")
    public ResponseEntity<Void> forgotPassword(@AuthenticationPrincipal User currentUser){
        
        User user = userRepository.findById(currentUser.getId()).get();
        user.setPassword(passwordEncoder.encode("123"));
        userRepository.save(user);
        
        SecurityContextHolder.getContext().getAuthentication().setAuthenticated(false);
        
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    @DeleteMapping("/delete")
    public ResponseEntity<Void> deleteUser(@AuthenticationPrincipal User currentUser){
        userRepository.delete(currentUser);
        
        SecurityContextHolder.getContext().getAuthentication().setAuthenticated(false);
        
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    
    
    
    
}
