package com.greemgnow.epicToDo.controller;

import com.greemgnow.epicToDo.dto.GoalDTO;
import com.greemgnow.epicToDo.entity.Goal;
import com.greemgnow.epicToDo.entity.User;
import com.greemgnow.epicToDo.repository.GoalRepository;
import com.greemgnow.epicToDo.repository.UserRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;


@RestController
@RequestMapping("goal")
public class GoalController {
    
    @Autowired
    GoalRepository goalRepository;
    
    @Autowired
    UserRepository userRepository;
    
    @GetMapping()
    public ResponseEntity<List<GoalDTO>> getGoalList(@AuthenticationPrincipal User currentUser){
        List<Goal> goals = goalRepository.findByUser(currentUser);
        List<GoalDTO> goalDTOs = new ArrayList<>();
        
        for(Goal goal : goals){
            goalDTOs.add(goal.toDTO());
        }
        
        return new ResponseEntity<>(goalDTOs, HttpStatus.OK);
    }
    
    @GetMapping("/{goalId}")
    public ResponseEntity<GoalDTO> getGoal(@PathVariable Long goalId, @AuthenticationPrincipal User currentUser){
        Optional<Goal> optGoal = goalRepository.findByIdAndUser(goalId, currentUser);
        
        if(!optGoal.isPresent())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            
        return new ResponseEntity<>(optGoal.get().toDTO(), HttpStatus.OK);
    }
    
    @PostMapping("/create")
    public ResponseEntity<GoalDTO> createGoal(@RequestBody GoalDTO goalDTO, @AuthenticationPrincipal User currentUser){
        Goal goal = new Goal();
        goal.fromDTO(goalDTO);
        goal.setUser(currentUser);
        
        return new ResponseEntity<>(goalRepository.save(goal).toDTO(), HttpStatus.CREATED);
    }
    
    @PutMapping("/edit")
    public ResponseEntity<GoalDTO> editGoal(@RequestBody GoalDTO goalDTO, @AuthenticationPrincipal User currentUser){
        Optional<Goal> optGoal = goalRepository.findByIdAndUser(goalDTO.getId(), currentUser);
        
        if(!optGoal.isPresent())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        Goal goal = optGoal.get();
        goal.fromDTO(goalDTO);
        
        return new ResponseEntity<>(goalRepository.save(goal).toDTO(), HttpStatus.OK);
    }
    
    @DeleteMapping("/delete/{goalId}")
    public ResponseEntity<Void> deleteGoal(@PathVariable Long goalId, @AuthenticationPrincipal User currentUser){
        Optional<Goal> optGoal = goalRepository.findByIdAndUser(goalId, currentUser);
        
        if(!optGoal.isPresent())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            
        goalRepository.deleteById(goalId);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    
    
}
