package com.greemgnow.epicToDo.controller;

import com.greemgnow.epicToDo.entity.Goal;
import com.greemgnow.epicToDo.entity.Quest;
import com.greemgnow.epicToDo.entity.User;
import com.greemgnow.epicToDo.repository.GoalRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.springframework.security.test.context.support.WithMockUser;
import com.greemgnow.epicToDo.repository.QuestRepository;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class QuestControllerTest {
    
    @MockBean
    private QuestRepository questRepositoryMock;
    
    @MockBean
    private GoalRepository goalRepositoryMock;
    
    @Autowired
    private MockMvc mockMvc;
    
    private final String JSON_QUESTDTO_1 = "{\"id\":\"1\",\"description\":\"quest1_description\",\"content\":\"quest1_content\",\"active\":\"true\",\"burnDate\":\"2019-02-07T12:00:00\",\"goal_id\":\"999\"}";
    private final String JSON_QUESTDTO_EMPTY = "{}";
    
    @Before
    public void setUp() throws Exception{
        
        Goal goal1 = new Goal(999L, "gial1_title", "goal1_description", true, null);
        
        Quest quest1 = new Quest(1L, "quest1_description", "quest1_content", true, 10, new Date(), null, goal1);
        Quest quest2 = new Quest(2L, "quest2_description", "quest2_content", false, 20, new Date(), null, null);
        
        List<Quest> questList = new ArrayList<>();
        questList.add(quest1);
        questList.add(quest2);
        
        Mockito
                .when(questRepositoryMock.findByUser(org.mockito.Matchers.any(User.class)))
                .thenReturn(questList);
        Mockito
                .when(questRepositoryMock.findByIdAndUser(eq(1L), org.mockito.Matchers.any(User.class)))
                .thenReturn(Optional.of(quest1));        
        Mockito
                .when(questRepositoryMock.findByIdAndUser(eq(0L), org.mockito.Matchers.any(User.class)))
                .thenReturn(Optional.empty());
        Mockito
                .when(questRepositoryMock.save(org.mockito.Matchers.any(Quest.class)))
                .thenReturn(quest1);
        Mockito
                .doNothing()
                .when(questRepositoryMock).deleteById(anyLong());
        
        Mockito
                .when(goalRepositoryMock.findByIdAndUser(eq(999L), org.mockito.Matchers.any(User.class)))
                .thenReturn(Optional.of(goal1));
        
        
    }
    
    @Test
    @WithMockCustomUser
    public void getQuestList_ifQuestFound_ThenReturnListOfQuestDTOAndHTTPStatusOK() throws Exception{
        mockMvc.perform(get("/quest"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].description", is("quest1_description")))
                .andExpect(jsonPath("$[0].content", is("quest1_content")))
                .andExpect(jsonPath("$[0].active", is(true)))
                .andExpect(jsonPath("$[0].goal_id", is(999)))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].description", is("quest2_description")))
                .andExpect(jsonPath("$[1].content", is("quest2_content")))
                .andExpect(jsonPath("$[1].active", is(false)))
                .andExpect(jsonPath("$[1].goal_id", is(nullValue())));
        
        verify(questRepositoryMock).findByUser(org.mockito.Matchers.any(User.class));
    }
    
    @Test
    @WithMockUser //deal WhithMockuser
    public void getQuestList_ifQuestNotFound_ThenReturnEmpryListOfQuestDTOAndHTTPStatusOK() throws Exception{
        mockMvc.perform(get("/quest"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(0)));
        
        verify(questRepositoryMock).findByUser(null);
    }
    
    @Test
    @WithMockCustomUser
    public void getQuest_ifQuestFound_ThenReturnQuestDTOAndHTTPStatusOK() throws Exception{
        mockMvc.perform(get("/quest/{questId}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.description", is("quest1_description")))
                .andExpect(jsonPath("$.content", is("quest1_content")))
                .andExpect(jsonPath("$.active", is(true)))
                .andExpect(jsonPath("$.goal_id", is(999)));
        
        verify(questRepositoryMock).findByIdAndUser(eq(1L), org.mockito.Matchers.any(User.class));
    }
    
    @Test
    @WithMockCustomUser
    public void getQuest_ifQuestNotFound_ThenReturnEmptyContentAndHTTPStatusNOTFOUND() throws Exception{
        mockMvc.perform(get("/quest/{questId}", 0))
                .andExpect(status().isNotFound())
                .andExpect(content().bytes(new byte[0]));
        
        verify(questRepositoryMock).findByIdAndUser(eq(0L), org.mockito.Matchers.any(User.class));
    }
    
    @Test
    @WithMockCustomUser
    public void createQuest_ifQuestDTONotEmpty_ThenCreateQuestAndReturnQuestDTOAndHTTPStatusCREATED() throws Exception{
        mockMvc.perform(post("/quest/create").contentType(MediaType.APPLICATION_JSON).content(JSON_QUESTDTO_1))
                .andExpect(status().isCreated())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.description", is("quest1_description")))
                .andExpect(jsonPath("$.content", is("quest1_content")))
                .andExpect(jsonPath("$.active", is(true)))
                .andExpect(jsonPath("$.goal_id", is(999)));
        
        verify(questRepositoryMock).save(org.mockito.Matchers.any(Quest.class));
    }
    
    @Test
    @WithMockCustomUser
    public void createQuest_ifQuestDTOIsEmpty_ThenCreateQuestAndReturnQuestDTOAndHTTPStatusCREATED() throws Exception{
        mockMvc.perform(post("/quest/create").contentType(MediaType.APPLICATION_JSON).content(JSON_QUESTDTO_EMPTY))
                .andExpect(status().isCreated())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.description", is("quest1_description")))
                .andExpect(jsonPath("$.content", is("quest1_content")))
                .andExpect(jsonPath("$.active", is(true)))
                .andExpect(jsonPath("$.goal_id", is(999)));
        
        verify(questRepositoryMock).save(org.mockito.Matchers.any(Quest.class));
    }
    
    @Test
    @WithMockCustomUser
    public void editQuest_ifQuestFound_ThenEditQuestAndReturnQuestDTOAndHTTPStatusOK() throws Exception{
        mockMvc.perform(put("/quest/edit").contentType(MediaType.APPLICATION_JSON).content(JSON_QUESTDTO_1))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.description", is("quest1_description")))
                .andExpect(jsonPath("$.content", is("quest1_content")))
                .andExpect(jsonPath("$.active", is(true)))
                .andExpect(jsonPath("$.goal_id", is(999)));
        
        verify(questRepositoryMock).save(org.mockito.Matchers.any(Quest.class));
    }
    
    @Test
    @WithMockCustomUser
    public void editQuest_ifQuestNotFound_ThenEditQuestReturnEmptyContentAndHTTPStatusNOTFOUND() throws Exception{
        mockMvc.perform(put("/quest/edit").contentType(MediaType.APPLICATION_JSON).content(JSON_QUESTDTO_EMPTY))
                .andExpect(status().isNotFound())
                .andExpect(content().bytes(new byte[0]));
        
        verify(questRepositoryMock, never()).save(org.mockito.Matchers.any(Quest.class));
    }
    
    @Test
    @WithMockCustomUser
    public void deleteQuest_ifQuestFound_ThenDeleteQuestAndReturnEmptyContentAndHTTPStatusNOCONTENT() throws Exception{
        mockMvc.perform(delete("/quest/delete/{questId}", 1))
                .andExpect(status().isNoContent())
                .andExpect(content().bytes(new byte[0]));
        
        verify(questRepositoryMock).deleteById(eq(1L));
    }
    
    @Test
    @WithMockCustomUser
    public void deleteQuest_ifQuestNotFound_ThenNotDeleteQuestAndReturnEmptyContentAndHTTPStatusNOTFOUND() throws Exception{
        mockMvc.perform(delete("/quest/delete/{questId}", 0))
                .andExpect(status().isNotFound())
                .andExpect(content().bytes(new byte[0]));
        
        verify(questRepositoryMock, never()).deleteById(anyLong());
    }
    
}
