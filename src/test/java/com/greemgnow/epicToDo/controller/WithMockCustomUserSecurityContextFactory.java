package com.greemgnow.epicToDo.controller;

import com.greemgnow.epicToDo.entity.User;
import java.util.ArrayList;
import java.util.List;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithSecurityContextFactory;


public class WithMockCustomUserSecurityContextFactory implements WithSecurityContextFactory<WithMockCustomUser> {
    @Override
	public SecurityContext createSecurityContext(WithMockCustomUser customUser) {
		SecurityContext context = SecurityContextHolder.createEmptyContext();
                
                /* Если нужны конкретные права
                List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
                grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_USER"));
                */
                
		User principal =
                        new User(1L, customUser.username(), "user1_password", customUser.name(), "user1_email", 0);
                
		Authentication auth =
			new UsernamePasswordAuthenticationToken(principal, principal.getPassword(), principal.getAuthorities());
		context.setAuthentication(auth);
		return context;
	}

}
